public class Exercise1 {

    public void question1(Account account2){
        System.out.println("Chạy bài 1");
        // Kiểm tra account thứ 2
        //Nếu không có phòng ban (tức là department == null) thì sẽ in ra text
        //"Nhân viên này chưa có phòng ban"
        //Nếu không thì sẽ in ra text "Phòng ban của nhân viên này là ..."

        // logic làm bài 1
        if (account2.department == null){
            System.out.println("Nhân viên này chưa có phòng ban");
        } else {
            System.out.println("Phòng ban của nhân viên này là: " + account2.department.departmentName);
        }

    }

    public void question2(Account account2){
        System.out.println("Chạy bài 2");
        int size = account2.groups.length;
        if (size==0){
            System.out.println("Nhân viên này chưa có group");
        } else if (size==1 || size==2){
            System.out.println("Group của nhân viên này là Java Fresher, C# Fresher");
        } else if(size == 3){
            System.out.println("Nhân viên này là người quan trọng, tham gia nhiều group ");
        } else if (size == 4){
            System.out.println("Nhân viên này là người hóng chuyện, tham gia tất cả các group");
        } else {
            System.out.println("Các trường hợp còn lại");
        }
    }

    public void question3(){
        System.out.println("Chạy bài 3");
    }
}
