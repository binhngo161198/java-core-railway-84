import java.util.Date;

public class Student {
    // msv, name, adress
    int id;
    String msv;
    String fullName;
    String userName;
    Date birthDay;
    boolean isPassCourse;
    Gender gender;
    float[] diem;

    // Tạo ra 1 chức năng để in ra điểm
    // trung bình cộng của sinh viên đó
    public float tinhDiem(){
        int size = diem.length; // số phần tử có trong danh sách điểm. VD = 3

        float diem1 = diem[0]; // Điểm đầu tiên trong danh sách
        float diem2 = diem[1];
        float diem3 = diem[2];

        float diemTB = (diem1 + diem2 + diem3)/3;
        return diemTB;
    }

    // Tạ ra 1 method đầu vào là 2 số, và kết quả là in ra 2 số đó
    public void testInput(int number1, int number2){// (khai báo kiểu dữ liệu và tên các biến truyền vào)
        System.out.println(number1 + number2);
    }

    // write, listen,...
    public void write(){
        // code here
        System.out.println("Đã vào đây");
        System.out.println(msv);
    }


    public int demoMethod(){
        // Code here
        return 5;
    }



    // public: Khả năng truy cập của method trong project. (4 giá trị)
    // void: Kiểu dữ liệu trả về khi gọi method, void: Không trả về gì cả


}
