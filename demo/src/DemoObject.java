import java.util.Arrays;
import java.util.Date;

public class DemoObject {
    public static void main(String[] args) {
        // (Kiểu dữ liệu) (tên biến) = (khởi tạo/ gán giá trị cho biến)
        // Sự khác nhau giữa Object và primitive: Object khởi tạo giá trị, primitive gán giá trị

        Student student1 = new Student(); // Khởi tạo giá trị cho biến student1
        // có kiểu dữ liệu là Student, giá trị là new Student()
//        student1.id = 1;
//        student1.msv = "2400000";
        student1.fullName = "Nguyễn Văn A";
//        student1.birthDay = new Date();
        float[] diemStudent1 = new float[3];
        diemStudent1[0] = 7;
        diemStudent1[1] = 7;
        diemStudent1[2] = 8;
        student1.diem = diemStudent1;// C1: Tạo 1 biến chứa các giá trị rồi gán
        student1.diem = new float[]{7.6f, 8f, 6f}; // C2: Gán trực tiếp



        // Bài toán: In ra thông tin msv của student1
        System.out.println( Arrays.toString(student1.diem) );


    }
}
